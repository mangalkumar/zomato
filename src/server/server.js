const express =  require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const sgMail = require('@sendgrid/mail')

const app = express()

const env = require('./config')
const user = require('../api/users/routes')
const sendOtp = require('../api/emails/sendOtp')
const restaurants = require('../api/restaurants/routes')

const port = env.PORT

app.use(cors())
app.use(express.urlencoded({extended:false}))
app.use(bodyParser.json())

sgMail
.setApiKey(env.SENDGRID_API_KEY) 

app.get('/', (req, res) => {
    res.send('welcome Jomato ')
})

app.use('/', user)
app.use('/', sendOtp)
app.use('/', restaurants)

app.use('*', (req, res)=> {
    res.status(404).json({
        message: "Page not found!. Try another page."
    })
})

app.listen(port, ()=>{
    console.log(`Server started at Port: ${port}`)
})