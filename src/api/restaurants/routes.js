const express = require('express')

const router = express.Router()
const { QueryTypes } = require('sequelize');

const joi = require('joi')
const bcrypt = require('bcrypt')
const Sequelize = require('sequelize')
const jwt = require('jsonwebtoken')

const db_connection = require('../../../Database/connection')

const FoodCategories = require('../../../Database/Models/foodCategories')
const Restaurants = require('../../../Database/Models/restaurants')

router.get('/food-categories', async (req, res) => {

    try {
        const foodCategories = await FoodCategories(db_connection, Sequelize).findAll({
            attributes: ['id', 'name']
        })

        res.json({
            status: "ok",
            data: foodCategories
        })

    } catch (error) {
        res.status(500).json({
            message: "Some error occurred.",
            error: error.message
        })
    }
})

router.get('/restaurants-details/:id', async (req, res) => {

    try {
        const restaurantsDetails = await db_connection.query(`select r.name as res, fc.name as category , fi.* from restaurants r inner join restaurants_food_categories_food_items rfc on r.id=rfc.restaurantId inner join food_categories fc on rfc.foodCategoryId = fc.id inner join food_items fi on rfc.foodItemId = fi.id where r.id = ${req.params.id};`,
            { type: QueryTypes.SELECT }
        )
        res.json({
            status: "ok",
            data: restaurantsDetails
        })

    } catch (error) {
        res.status(500).json({
            message: "Some error occurred.",
            error: error.message
        })
    }
})

router.get('/restaurants/:url', async (req, res) => {
    try {
        const restaurants = await Restaurants(db_connection, Sequelize).findAll({
            where: {
                url: req.params.url
            }
        })

        res.json({
            status: "ok",
            data: restaurants[0]
        })

    } catch (error) {
        res.status(500).json({
            message: "Some error occurred.",
            error: error.message
        })
    }
})

router.get('/restaurants', async (req, res) => {
    try {
        const restaurants = await Restaurants(db_connection, Sequelize).findAll()

        res.json({
            status: "ok",
            data: restaurants
        })

    } catch (error) {
        res.status(500).json({
            message: "Some error occurred.",
            error: error.message
        })
    }
})

router.get('/food-items', async (req, res) => {

    try {
        const restaurantsDetails = await db_connection.query(`select r.name as restaurantName, r.url as restaurantUrl, fc.name as category , fi.* from restaurants r inner join restaurants_food_categories_food_items rfc on r.id=rfc.restaurantId inner join food_categories fc on rfc.foodCategoryId = fc.id inner join food_items fi on rfc.foodItemId = fi.id`,
            { type: QueryTypes.SELECT }
        )
        res.json({
            status: "ok",
            data: restaurantsDetails
        })

    } catch (error) {
        res.status(500).json({
            message: "Some error occurred.",
            error: error.message
        })
    }
})


module.exports = router