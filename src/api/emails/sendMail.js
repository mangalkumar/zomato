const sgMail = require('@sendgrid/mail')

function sendMail(receiverEmail, subject, message) {

    return new Promise((resolve, reject) => {
        const data = {
            to: receiverEmail,
            from: 'Jomato <mangalkumar8@gmail.com>',
            subject: subject,
            html: message,
        }

        sgMail
            .send(data)
            .then((result) => {
                return resolve(result)
            })
            .catch((error) => {
                return reject(error)
            })
    })
}

module.exports = sendMail