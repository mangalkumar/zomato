const express = require('express')
const sendMail = require('./sendMail')
const joi = require('joi')

const router = express.Router()

router.post('/sendOtp', async (req, res) => {

    try {
        const schema = joi.object().keys({
            email: joi.string().email().required(),
            message: joi.string().required()
        })

        const result = await schema.validate(req.body)

        if(result.error != undefined){
            return res.status(400).json({
                message: result.error.message
            })
        }
        
        const {email, message} = req.body
        const subject = "OTP Verification for Jomato Account."

        await sendMail(email, subject, message)

        res.json({
            status: 'success',
            message: "OTP sent to your Email-Id.",
        })

    } catch (err) { 
        console.log(err, "While sending otp, error occurred.")
        res.json({
            status: 'error',
            message: "While sending otp, An error occurred. Please try again later.",
        })
    }
})

module.exports = router