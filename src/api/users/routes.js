const express = require('express')

const router = express.Router()

const joi = require('joi')
const bcrypt = require('bcrypt')
const Sequelize = require('sequelize')
const jwt = require('jsonwebtoken')

const db_connection = require('../../../Database/connection')

const User = require('../../../Database/Models/users')

router.post('/login', async (req, res) => {

    const schema = joi.object().keys({
        email: joi.string().email().required(),
        password: joi.string().required()
    })

    const result = schema.validate(req.body)

    if (result.error == undefined) {
        try{
            const { email, password } = req.body

            const queryResult = await User(db_connection, Sequelize).findAll({
                where: {
                    email: email
                }
            })
    
            if (queryResult.length === 0) {
                res.json({
                    message: "Invalid details , Please try again.",
                })
            } else {
                const { password: hashedPassword, firstName } = queryResult[0]
    
                bcrypt.compare(password, hashedPassword, function (err, result) {
    
                    if (result == false) {
                        res.json({
                            message: "Wrong Password , Please try again.",
                        })
                    } else {
                        const user = {
                            email: email,
                            firstName
                        }
                        const token = jwt.sign({
                            data: user
                        }, 'secretKey', { expiresIn: '1h' })
    
                        res.json({
                            user,
                            token,
                            message: 'Logged In, successfully.'
                        })
                    }
                })
            }
        }catch(error){
            res.status(500).json({
                message: "Some error occurred.",
                error: error.message
            })
        }
    } else {
        res.status(400).json({
            message: result.error.message
        })
    }
})


router.post('/signup', async (req, res) => {
    const schema = joi.object().keys({
        firstName: joi.string().min(3).required(),
        lastName: joi.string().min(3),
        email: joi.string().email().required(),
        password: joi.string().required(),
    })

    const result = schema.validate(req.body)

    if (result.error == undefined) {
        try {
            const { firstName, lastName, email, password } = req.body

            const result = await User(db_connection, Sequelize).findAll({
                where: {
                    email: email
                }
            })

            if (result.length === 1) {
                return res.json({
                    message: "Email already exist."
                })
            }

            const hashedPassword = await bcrypt.hash(password, saltRounds = 10)

            await User(db_connection, Sequelize).create({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: hashedPassword,
            })

            res.json({
                message: "Signed up successfully."
            })

        } catch (error) {
            res.status(500).json({
                message: "While registering, Some error occurred. Please try again later.",
                error: error.message
            })
        }
    } else {
        console.log(result.error.message)
        res.status(400).json({
            message: result.error.message
        })
    }
})

router.post('/check-email', async (req, res) => {
    const schema = joi.object().keys({
        email: joi.string().email().required(),
    })

    const result = schema.validate(req.body)

    if (result.error == undefined) {
        try {
            const {email} = req.body

            const result = await User(db_connection, Sequelize).findAll({
                where: {
                    email: email
                }
            })
            if (result.length === 1) {
                return res.status(200).json({
                    status: "error",
                    message: "Email already exist."
                })
            }else{
                return res.status(200).json({
                    status: "success",
                    message: "Email is available."
                })
            }
        }catch(error){
            console.log(error)
            return res.status(200).json({
                message: "While checking your email an error occurred. Please try again later."
            })
        }
    }else{
        return res.status(200).json({
            message: "While checking your email an error occurred. Please try again later."
        })
    }
})

module.exports = router