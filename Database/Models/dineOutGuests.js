const DineOutGuests = (db_connection, Sequelize) => (db_connection.define('dine_out_guests', {
  id: {
    type: Sequelize.DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  dineOutId: {
    type: Sequelize.DataTypes.INTEGER,
    references: {
      model: 'dine_out',
      key: "id"
    },
    allowNull: false
  },
  guestName: {
    type: Sequelize.DataTypes.STRING,
    allowNull: false
  },
  email: {
    type: Sequelize.DataTypes.STRING,
    allowNull: false
  },
  phoneNumber: {
    type: Sequelize.DataTypes.STRING,
    allowNull: false
  }
}))

module.exports = DineOutGuests