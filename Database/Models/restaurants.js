const Restaurants = (db_connection, Sequelize) => (db_connection.define("restaurants", {
    id: {
        type: Sequelize.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false
    },
    address: {
        type: Sequelize.DataTypes.TEXT,
        allowNull: false
    },
    imageUrl: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false
    },
    url: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
}))

module.exports = Restaurants