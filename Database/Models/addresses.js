const Addresses = (db_connection, Sequelize) => (db_connection.define('addresses', {
  id: {
    type: Sequelize.DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  userId: {
    type: Sequelize.DataTypes.INTEGER,
    references: {
      model: 'users',
      key: "id"
    }
  },
  address: {
    type: Sequelize.DataTypes.TEXT,
    allowNull: false
  }
}))

module.exports = Addresses