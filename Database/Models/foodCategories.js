const FoodCategories = (db_connection, Sequelize) => (db_connection.define("food_categories", {
    id: {
        type: Sequelize.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false
    }
}))

module.exports = FoodCategories