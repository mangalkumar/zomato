const Reviews = (db_connection, Sequelize) => (db_connection.define('reviews', {
  id: {
    type: Sequelize.DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  userId: {
    type: Sequelize.DataTypes.INTEGER,
    references: {
      model: 'users',
      key: "id"
    },
  },
  restaurantId: {
    type: Sequelize.DataTypes.INTEGER,
    references: {
      model: 'restaurants',
      key: "id"
    },
  },
  reviewMessage: {
    type: Sequelize.DataTypes.TEXT,
    allowNull: false
  },
  rating: {
    type: Sequelize.DataTypes.INTEGER,
    allowNull: false
  }
}))

module.exports = Reviews