const Users = (db_connection, Sequelize )=> (db_connection.define("users", {
  id: {
      type: Sequelize.DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
  },
  firstName: {
      type: Sequelize.DataTypes.STRING,
      allowNull: false
  },
  lastName: {
      type: Sequelize.DataTypes.STRING,
      allowNull: true,
      defaultValue: ""
  },
  email: {
      type: Sequelize.DataTypes.STRING,
      allowNull: false
  },
  password: {
      type: Sequelize.DataTypes.STRING,
      allowNull: false
  }
}))

module.exports = Users