const FoodItems = (db_connection, Sequelize) => (db_connection.define("food_items", {
    id: {
        type: Sequelize.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: Sequelize.DataTypes.DOUBLE,
        allowNull: false
    },
    foodCategoryId: {
        type: Sequelize.DataTypes.INTEGER,
        references: {
            model: 'food_categories',
            key: 'id'
          },
        allowNull: false
    }
}))


module.exports = FoodItems
