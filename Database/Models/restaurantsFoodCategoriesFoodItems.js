const RestaurantsFoodCategoriesFoodItems = (db_connection, Sequelize) => (db_connection.define("restaurants_food_categories_food_items", {
    restaurantId: {
        type: Sequelize.DataTypes.INTEGER,
        references: {
            model: 'restaurants',
            key: "id"
          },
        allowNull: false
    },
    foodCategoryId: {
        type: Sequelize.DataTypes.INTEGER,
        references: {
            model: 'food_categories',
            key: "id"
          },
        allowNull: false
    },
    foodItemId: {
        type: Sequelize.DataTypes.INTEGER,
        references: {
            model: 'food_items',
            key: "id"
          },
        allowNull: false
    }
}))

module.exports = RestaurantsFoodCategoriesFoodItems