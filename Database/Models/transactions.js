const Transactions = (db_connection, Sequelize) => (db_connection.define('transactions', {
  id: {
    type: Sequelize.DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  orderId: {
    type: Sequelize.DataTypes.INTEGER,
    references: {
      model: 'orders',
      key: "id"
    },
    allowNull: false
  },
  userId: {
    type: Sequelize.DataTypes.INTEGER,
    references: {
      model: 'users',
      key: "id"
    },
    allowNull: false
  },
  amount: {
    type: Sequelize.DataTypes.DOUBLE,
    allowNull: false
  },
  paymentMethod: {
    type: Sequelize.DataTypes.STRING,
    allowNull: false
  }
}))

module.exports = Transactions