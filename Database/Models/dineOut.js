const DineOut = (db_connection, Sequelize) => (db_connection.define('dine_out', {
  id: {
    type: Sequelize.DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  date: {
    type: Sequelize.DataTypes.DATE,
  },
  restaurantId: {
    type: Sequelize.DataTypes.INTEGER,
    references: {
      model: 'restaurants',
      key: "id"
    }
  },
  userId: {
    type: Sequelize.DataTypes.INTEGER,
    references: {
      model: 'users',
      key: "id"
    }
  }
}))

module.exports = DineOut