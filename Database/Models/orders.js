const Orders = (db_connection, Sequelize) => (db_connection.define('orders', {
  id: {
    type: Sequelize.DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  userId: {
    type: Sequelize.DataTypes.INTEGER,
    references: {
      model: 'users',
      key: "id"
    },
    allowNull: false
  },
  date: {
    type: Sequelize.DataTypes.DATE,
    allowNull: false
  },
  deliveryAddress: {
    type: Sequelize.DataTypes.TEXT,
    allowNull: false
  }
}))

module.exports = Orders