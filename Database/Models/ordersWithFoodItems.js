const OrdersWithFoodItems = (db_connection, Sequelize) => (db_connection.define('orders_with_food_items', {
  orderId: {
    type: Sequelize.DataTypes.INTEGER,
    references: {
      model: 'orders',
      key: "id"
    },
    allowNull: false
  },
  foodItemId: {
    type: Sequelize.DataTypes.INTEGER,
    references: {
      model: 'food_items',
      key: "id"
    },
    allowNull: false
  },
  quantities: {
    type: Sequelize.DataTypes.INTEGER,
    allowNull: false
  }
}))

module.exports = OrdersWithFoodItems