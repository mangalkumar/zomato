const Sequelize = require('sequelize')

const db = require('./connection')

const Addresses = require('./Models/addresses')
const DineOut = require('./Models/dineOut')
const DineOutGuests = require('./Models/dineOutGuests')
const FoodCategories = require('./Models/foodCategories')
const Orders = require('./Models/orders')
const FoodItems = require('./Models/foodItems')
const OrdersWithFoodItems = require('./Models/ordersWithFoodItems')
const Restaurants = require('./Models/restaurants')
const RestaurantsFoodCategoriesFoodItems = require('./Models/restaurantsFoodCategoriesFoodItems')
const Reviews = require('./Models/reviews')
const Transactions = require('./Models/transactions')
const Users = require('./Models/users')


async function syncModels() {
    try {

        await db.authenticate()
        await Users(db, Sequelize).sync()
        await Addresses(db, Sequelize).sync()
        await FoodCategories(db, Sequelize).sync()
        await FoodItems(db, Sequelize).sync()
        await Orders(db, Sequelize).sync()
        await OrdersWithFoodItems(db, Sequelize).sync()
        await Restaurants(db, Sequelize).sync()
        await RestaurantsFoodCategoriesFoodItems(db, Sequelize).sync()
        await Reviews(db, Sequelize).sync()
        await Transactions(db, Sequelize).sync()
        await DineOut(db, Sequelize).sync()
        await DineOutGuests(db, Sequelize).sync()

    } catch (error) {
        console.log("While creating tables some error occurred: ", error)
    }
}

syncModels()