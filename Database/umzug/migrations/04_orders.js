// All migrations must provide a `up` and `down` async functions

const { Sequelize } = require('sequelize');

async function up(queryInterface) {
  await queryInterface.createTable('orders', {
    id: {
      type: Sequelize.DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    userId: {
      type: Sequelize.DataTypes.INTEGER,
      references: {
        model: 'users',
        key: "id"
      },
      allowNull: false
    },
    date: {
      type: Sequelize.DataTypes.DATE,
      allowNull: false
    },
    deliveryAddress: {
      type: Sequelize.DataTypes.TEXT,
      allowNull: false
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
  })
}

async function down(queryInterface) {
  await queryInterface.dropTable('orders');
}


module.exports = { up, down };