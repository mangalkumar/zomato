// All migrations must provide a `up` and `down` async functions

const { Sequelize } = require('sequelize');

async function up(queryInterface) {
    await queryInterface.createTable('restaurants_food_categories_food_items', {
        restaurantId: {
            type: Sequelize.DataTypes.INTEGER,
            references: {
                model: 'restaurants',
                key: "id"
            },
            allowNull: false
        },
        foodCategoryId: {
            type: Sequelize.DataTypes.INTEGER,
            references: {
                model: 'food_categories',
                key: "id"
            },
            allowNull: false
        },
        foodItemId: {
            type: Sequelize.DataTypes.INTEGER,
            references: {
                model: 'food_items',
                key: "id"
            },
            allowNull: false
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    })
}

async function down(queryInterface) {
    await queryInterface.dropTable('restaurants_food_categories_food_items');
}


module.exports = { up, down };