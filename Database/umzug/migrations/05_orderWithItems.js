// All migrations must provide a `up` and `down` async functions

const { Sequelize } = require('sequelize');

async function up(queryInterface) {
    await queryInterface.createTable('orders_with_food_items', {
        orderId: {
            type: Sequelize.DataTypes.INTEGER,
            references: {
                model: 'orders',
                key: "id"
            },
            allowNull: false
        },
        foodItemId: {
            type: Sequelize.DataTypes.INTEGER,
            references: {
                model: 'food_items',
                key: "id"
            },
            allowNull: false
        },
        quantities: {
            type: Sequelize.DataTypes.INTEGER,
            allowNull: false
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    })
}

async function down(queryInterface) {
    await queryInterface.dropTable('orders_with_food_items');
}


module.exports = { up, down };