// All migrations must provide a `up` and `down` async functions

const { Sequelize } = require('sequelize');

async function up(queryInterface) {
    await queryInterface.createTable('dine_out', {
        id: {
            type: Sequelize.DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        date: {
            type: Sequelize.DataTypes.DATE,
        },
        restaurantId: {
            type: Sequelize.DataTypes.INTEGER,
            references: {
                model: 'restaurants',
                key: "id"
            }
        },
        userId: {
            type: Sequelize.DataTypes.INTEGER,
            references: {
                model: 'users',
                key: "id"
            }
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    })
}

async function down(queryInterface) {
    await queryInterface.dropTable('dine_out');
}


module.exports = { up, down };