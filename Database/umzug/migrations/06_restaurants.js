// All migrations must provide a `up` and `down` async functions

const { Sequelize } = require('sequelize');

async function up(queryInterface) {
    await queryInterface.createTable('restaurants', {
        id: {
            type: Sequelize.DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        address: {
            type: Sequelize.DataTypes.TEXT,
            allowNull: false
        },
        imageUrl: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        url: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    })
}

async function down(queryInterface) {
    await queryInterface.dropTable('restaurants');
}


module.exports = { up, down };