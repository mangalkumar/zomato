// All migrations must provide a `up` and `down` async functions

const { Sequelize } = require('sequelize');

async function up(queryInterface) {
  await queryInterface.createTable('reviews', {
    id: {
      type: Sequelize.DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    userId: {
      type: Sequelize.DataTypes.INTEGER,
      references: {
        model: 'users',
        key: "id"
      },
    },
    restaurantId: {
      type: Sequelize.DataTypes.INTEGER,
      references: {
        model: 'restaurants',
        key: "id"
      },
    },
    reviewMessage: {
      type: Sequelize.DataTypes.TEXT,
      allowNull: false
    },
    rating: {
      type: Sequelize.DataTypes.INTEGER,
      allowNull: false
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
  })
}

async function down(queryInterface) {
  await queryInterface.dropTable('reviews');
}


module.exports = { up, down };