// All migrations must provide a `up` and `down` async functions
 
const { Sequelize } = require('sequelize');

async function up(queryInterface) {
	await queryInterface.createTable('addresses', {
		id: {
            type: Sequelize.DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
          },
          userId: {
            type: Sequelize.DataTypes.INTEGER,
            references: {
              model: 'users',
              key: "id"
            }
          },
          address: {
            type: Sequelize.DataTypes.TEXT,
            allowNull: false
          }
	});
}

async function down(queryInterface) {
	await queryInterface.dropTable('addresses');
}


module.exports = { up, down };