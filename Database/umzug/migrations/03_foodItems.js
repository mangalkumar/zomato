// All migrations must provide a `up` and `down` async functions

const { Sequelize } = require('sequelize');

async function up(queryInterface) {
    await queryInterface.createTable('food_items', {
        id: {
            type: Sequelize.DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        price: {
            type: Sequelize.DataTypes.DOUBLE,
            allowNull: false
        },
        foodCategoryId: {
            type: Sequelize.DataTypes.INTEGER,
            references: {
                model: 'food_categories',
                key: 'id'
            },
            allowNull: false
        },
        imageUrl: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    })
}

async function down(queryInterface) {
    await queryInterface.dropTable('food_items');
}


module.exports = { up, down };