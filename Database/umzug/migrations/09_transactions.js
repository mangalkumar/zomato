// All migrations must provide a `up` and `down` async functions

const { Sequelize } = require('sequelize');

async function up(queryInterface) {
    await queryInterface.createTable('transactions', {
        id: {
            type: Sequelize.DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        orderId: {
            type: Sequelize.DataTypes.INTEGER,
            references: {
                model: 'orders',
                key: "id"
            },
            allowNull: false
        },
        userId: {
            type: Sequelize.DataTypes.INTEGER,
            references: {
                model: 'users',
                key: "id"
            },
            allowNull: false
        },
        amount: {
            type: Sequelize.DataTypes.DOUBLE,
            allowNull: false
        },
        paymentMethod: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
    })
}

async function down(queryInterface) {
    await queryInterface.dropTable('transactions');
}


module.exports = { up, down };