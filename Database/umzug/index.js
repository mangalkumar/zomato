const Sequelize = require('sequelize')
const path = require('path')
const Umzug = require('umzug')

require('dotenv').config()

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
  host: process.env.DB_HOST,
  dialect: 'mysql',
  define: {
    timeStamp: false,
    freezeTableName: true
  }
})
const migrateUmzug = new Umzug({

  migrations: {
    // indicates the folder containing the migration .js files
    path: path.join(__dirname, './migrations'),
    // inject sequelize's QueryInterface in the migrations
    params: [
      sequelize.getQueryInterface()
    ]
  }
  // indicates that the migration data should be store in the database
  // itself through sequelize. The default configuration creates a table
  // named `SequelizeMeta`.
})

const seedUmzug = new Umzug({

  migrations: {
    // indicates the folder containing the migration .js files
    path: path.join(__dirname, './seeders'),
    // inject sequelize's QueryInterface in the migrations
    params: [
      sequelize.getQueryInterface()
    ]
  }
  // indicates that the migration data should be store in the database
  // itself through sequelize. The default configuration creates a table
  // named `SequelizeMeta`.
})

;(async () => {
  // checks migrations and run them if they are not already applied
  try{
    await migrateUmzug.up()
    await seedUmzug.up()

  }catch(error){
      console.log(error)
  }
  console.log('All seeding performed successfully')
})()
