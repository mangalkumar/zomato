// All migrations must provide a `up` and `down` async functions
 
const { Sequelize } = require('sequelize');

async function up(queryInterface) {
    console.log("okkkokok")
	await queryInterface.createTable('users', {
		id: {
            type: Sequelize.DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        firstName: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        lastName: {
            type: Sequelize.DataTypes.STRING,
            allowNull: true,
            defaultValue: ""
        },
        email: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        },
        password: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false
        }
	});
}

async function down(queryInterface) {
	await queryInterface.dropTable('users');
}


module.exports = { up, down };