const Sequelize = require('sequelize')

// All migrations must provide a `up` and `down` async functions

module.exports = {
    // `query` was passed in the `index.js` file
    up: async (queryInterface) => {
        const date = new Date();
        await queryInterface.bulkInsert(
            'restaurants',
            [
                {
                    id :1,
                    name: 'Blue Nile',
                    address:"Camp Area, Pune",
                    imageUrl: "https://b.zmtcdn.com/data/pictures/7/10007/97eb9ac98ce6ffbe25d7ccee4e36c83a.jpg?output-format=webp&fit=around|771.75:416.25&crop=771.75:416.25;*,*",
                    url: "blue-nile"
                },
                {
                    id :2,
                    name: 'Vaishali',
                    address:"FC Road, Pune",
                    imageUrl: "https://b.zmtcdn.com/data/pictures/5/10125/9b82ae2f1aecabc6af5bd66cc97641a2.jpg?output-format=webp&fit=around|771.75:416.25&crop=771.75:416.25;*,*",
                    url: "vaishali"
                },
                {
                    id :3,
                    name: 'Mummy\'s Cafe',
                    address:"Kothrud Area, Pune",
                    imageUrl: "https://b.zmtcdn.com/data/pictures/chains/7/19519957/3e0adb804dc92f5149be47188015351f.jpg?fit=around|771.75:416.25&crop=771.75:416.25;*,*",
                    url: "mummy-cafe"
                },
                {
                    id :4,
                    name: "Madurai Idly Shop",
                    address:"Kothrud Area, Pune",
                    imageUrl: "https://b.zmtcdn.com/data/pictures/6/19549586/903a749f5fc584403ae9e59fefddbc2a.jpg?fit=around|771.75:416.25&crop=771.75:416.25;*,*",
                    url: "madurai-idly-shop"
                },
                {
                    id :5,
                    name: "Paratha Experiment",
                    address:"Kothrud Area, Pune",
                    imageUrl: "https://b.zmtcdn.com/data/pictures/5/19343115/4eaa901f211f59c7e7f59e66862cd883.jpg?fit=around|771.75:416.25&crop=771.75:416.25;*,*",
                    url: "paratha-experiment"
                },
                {
                    id :6,
                    name: 'Blue Nile',
                    address:"Camp Area, Pune",
                    imageUrl: "https://b.zmtcdn.com/data/pictures/7/10007/97eb9ac98ce6ffbe25d7ccee4e36c83a.jpg?output-format=webp&fit=around|771.75:416.25&crop=771.75:416.25;*,*",
                    url: "blue-nile"
                },
                {
                    id :7,
                    name: 'Vaishali',
                    address:"FC Road, Pune",
                    imageUrl: "https://b.zmtcdn.com/data/pictures/5/10125/9b82ae2f1aecabc6af5bd66cc97641a2.jpg?output-format=webp&fit=around|771.75:416.25&crop=771.75:416.25;*,*",
                    url: "vaishali"
                },
                {
                    id :8,
                    name: 'Mummy\'s Cafe',
                    address:"Kothrud Area, Pune",
                    imageUrl: "https://b.zmtcdn.com/data/pictures/chains/7/19519957/3e0adb804dc92f5149be47188015351f.jpg?fit=around|771.75:416.25&crop=771.75:416.25;*,*",
                    url: "mummy-cafe"
                },
                {
                    id :9,
                    name: "Madurai Idly Shop",
                    address:"Kothrud Area, Pune",
                    imageUrl: "https://b.zmtcdn.com/data/pictures/6/19549586/903a749f5fc584403ae9e59fefddbc2a.jpg?fit=around|771.75:416.25&crop=771.75:416.25;*,*",
                    url: "madurai-idly-shop"
                },
                {
                    id :10,
                    name: "Paratha Experiment",
                    address:"Kothrud Area, Pune",
                    imageUrl: "https://b.zmtcdn.com/data/pictures/5/19343115/4eaa901f211f59c7e7f59e66862cd883.jpg?fit=around|771.75:416.25&crop=771.75:416.25;*,*",
                    url: "paratha-experiment"
                },
                
                
            ],
            {}
        )
    },
    down: async (query) => {
        await query.dropTable('restaurants')
    }
}