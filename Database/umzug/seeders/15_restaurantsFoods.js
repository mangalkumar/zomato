const Sequelize = require('sequelize')

// All migrations must provide a `up` and `down` async functions

module.exports = {
    // `query` was passed in the `index.js` file
    up: async (queryInterface) => {
        const date = new Date();
        await queryInterface.bulkInsert(
            'restaurants_food_categories_food_items',
            [
                {
                    restaurantId: 1,
                    foodCategoryId: 1,
                    foodItemId: 1
                },
                {
                    restaurantId: 1,
                    foodCategoryId: 1,
                    foodItemId: 2
                },
                {
                    restaurantId: 1,
                    foodCategoryId: 2,
                    foodItemId: 6
                },
                {
                    restaurantId: 1,
                    foodCategoryId: 2,
                    foodItemId: 7
                },
                {
                    restaurantId: 1,
                    foodCategoryId: 2,
                    foodItemId: 8
                },
                {
                    restaurantId: 1,
                    foodCategoryId: 3,
                    foodItemId: 9
                },
                {
                    restaurantId: 1,
                    foodCategoryId: 3,
                    foodItemId: 9
                },
                {
                    restaurantId: 1,
                    foodCategoryId: 3,
                    foodItemId: 10
                },
                {
                    restaurantId: 1,
                    foodCategoryId: 3,
                    foodItemId: 11
                },
                {
                    restaurantId: 1,
                    foodCategoryId: 4,
                    foodItemId: 12
                },
                {
                    restaurantId: 1,
                    foodCategoryId: 4,
                    foodItemId: 13
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 1,
                    foodItemId: 1
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 1,
                    foodItemId: 2
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 2,
                    foodItemId: 6
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 2,
                    foodItemId: 7
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 2,
                    foodItemId: 8
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 3,
                    foodItemId: 9
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 3,
                    foodItemId: 9
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 3,
                    foodItemId: 10
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 3,
                    foodItemId: 11
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 4,
                    foodItemId: 12
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 4,
                    foodItemId: 13
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 5,
                    foodItemId: 14
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 5,
                    foodItemId: 15
                },
                {
                    restaurantId: 2,
                    foodCategoryId: 6,
                    foodItemId: 17
                },
                {
                    restaurantId: 3,
                    foodCategoryId: 3,
                    foodItemId: 11
                },
                {
                    restaurantId: 3,
                    foodCategoryId: 4,
                    foodItemId: 12
                },
                {
                    restaurantId: 3,
                    foodCategoryId: 4,
                    foodItemId: 13
                },
                {
                    restaurantId: 3,
                    foodCategoryId: 5,
                    foodItemId: 14
                },
                {
                    restaurantId: 3,
                    foodCategoryId: 5,
                    foodItemId: 15
                },
                {
                    restaurantId: 3,
                    foodCategoryId: 6,
                    foodItemId: 17
                },
                {
                    restaurantId: 3,
                    foodCategoryId: 7,
                    foodItemId: 19
                },
                {
                    restaurantId: 3,
                    foodCategoryId: 7,
                    foodItemId: 20
                },
                {
                    restaurantId: 3,
                    foodCategoryId: 8,
                    foodItemId: 21
                },
                {
                    restaurantId: 4,
                    foodCategoryId: 3,
                    foodItemId: 9
                },
                {
                    restaurantId: 4,
                    foodCategoryId: 3,
                    foodItemId: 10
                },
                {
                    restaurantId: 4,
                    foodCategoryId: 3,
                    foodItemId: 11
                },
                {
                    restaurantId: 4,
                    foodCategoryId: 4,
                    foodItemId: 12
                },
                {
                    restaurantId: 4,
                    foodCategoryId: 4,
                    foodItemId: 13
                },
                {
                    restaurantId: 4,
                    foodCategoryId: 5,
                    foodItemId: 14
                },
                {
                    restaurantId: 4,
                    foodCategoryId: 5,
                    foodItemId: 15
                },
                {
                    restaurantId: 4,
                    foodCategoryId: 5,
                    foodItemId: 16
                },
                {
                    restaurantId: 4,
                    foodCategoryId: 6,
                    foodItemId: 17
                },
                {
                    restaurantId: 4,
                    foodCategoryId: 7,
                    foodItemId: 19
                },
                {
                    restaurantId: 4,
                    foodCategoryId: 7,
                    foodItemId: 20
                },
                {
                    restaurantId: 4,
                    foodCategoryId: 8,
                    foodItemId: 21
                },
                {
                    restaurantId: 5,
                    foodCategoryId: 3,
                    foodItemId: 9
                },
                {
                    restaurantId: 5,
                    foodCategoryId: 3,
                    foodItemId: 10
                },
                {
                    restaurantId: 5,
                    foodCategoryId: 3,
                    foodItemId: 11
                },
                {
                    restaurantId: 5,
                    foodCategoryId: 4,
                    foodItemId: 12
                },
                {
                    restaurantId: 5,
                    foodCategoryId: 4,
                    foodItemId: 13
                },
                {
                    restaurantId: 5,
                    foodCategoryId: 5,
                    foodItemId: 14
                },
                {
                    restaurantId: 5,
                    foodCategoryId: 5,
                    foodItemId: 15
                },
                {
                    restaurantId: 5,
                    foodCategoryId: 5,
                    foodItemId: 16
                },
                {
                    restaurantId: 5,
                    foodCategoryId: 6,
                    foodItemId: 17
                },
                {
                    restaurantId: 5,
                    foodCategoryId: 7,
                    foodItemId: 19
                },
                {
                    restaurantId: 5,
                    foodCategoryId: 7,
                    foodItemId: 20
                },
                {
                    restaurantId: 5,
                    foodCategoryId: 8,
                    foodItemId: 21
                },
                {
                    restaurantId: 6,
                    foodCategoryId: 3,
                    foodItemId: 9
                },
                {
                    restaurantId: 6,
                    foodCategoryId: 3,
                    foodItemId: 10
                },
                {
                    restaurantId: 6,
                    foodCategoryId: 3,
                    foodItemId: 11
                },
                {
                    restaurantId: 6,
                    foodCategoryId: 4,
                    foodItemId: 12
                },
                {
                    restaurantId: 6,
                    foodCategoryId: 4,
                    foodItemId: 13
                },
                {
                    restaurantId: 6,
                    foodCategoryId: 5,
                    foodItemId: 14
                },
                {
                    restaurantId: 6,
                    foodCategoryId: 5,
                    foodItemId: 15
                },
                {
                    restaurantId: 6,
                    foodCategoryId: 5,
                    foodItemId: 16
                },
                {
                    restaurantId: 6,
                    foodCategoryId: 6,
                    foodItemId: 17
                },
                {
                    restaurantId: 6,
                    foodCategoryId: 7,
                    foodItemId: 19
                },
                {
                    restaurantId: 6,
                    foodCategoryId: 7,
                    foodItemId: 20
                },
                {
                    restaurantId: 6,
                    foodCategoryId: 8,
                    foodItemId: 21
                },
                {
                    restaurantId: 7,
                    foodCategoryId: 3,
                    foodItemId: 9
                },
                {
                    restaurantId: 7,
                    foodCategoryId: 3,
                    foodItemId: 10
                },
                {
                    restaurantId: 7,
                    foodCategoryId: 3,
                    foodItemId: 11
                },
                {
                    restaurantId: 7,
                    foodCategoryId: 4,
                    foodItemId: 12
                },
                {
                    restaurantId: 7,
                    foodCategoryId: 4,
                    foodItemId: 13
                },
                {
                    restaurantId: 7,
                    foodCategoryId: 5,
                    foodItemId: 14
                },
                {
                    restaurantId: 7,
                    foodCategoryId: 5,
                    foodItemId: 15
                },
                {
                    restaurantId: 7,
                    foodCategoryId: 5,
                    foodItemId: 16
                },
                {
                    restaurantId: 7,
                    foodCategoryId: 6,
                    foodItemId: 17
                },
                {
                    restaurantId: 7,
                    foodCategoryId: 7,
                    foodItemId: 19
                },
                {
                    restaurantId: 7,
                    foodCategoryId: 7,
                    foodItemId: 20
                },
                {
                    restaurantId: 7,
                    foodCategoryId: 8,
                    foodItemId: 21
                },
                {
                    restaurantId: 8,
                    foodCategoryId: 3,
                    foodItemId: 9
                },
                {
                    restaurantId: 8,
                    foodCategoryId: 3,
                    foodItemId: 10
                },
                {
                    restaurantId: 8,
                    foodCategoryId: 3,
                    foodItemId: 11
                },
                {
                    restaurantId: 8,
                    foodCategoryId: 4,
                    foodItemId: 12
                },
                {
                    restaurantId: 8,
                    foodCategoryId: 4,
                    foodItemId: 13
                },
                {
                    restaurantId: 8,
                    foodCategoryId: 5,
                    foodItemId: 14
                },
                {
                    restaurantId: 8,
                    foodCategoryId: 5,
                    foodItemId: 15
                },
                {
                    restaurantId: 8,
                    foodCategoryId: 5,
                    foodItemId: 16
                },
                {
                    restaurantId: 8,
                    foodCategoryId: 6,
                    foodItemId: 17
                },
                {
                    restaurantId: 8,
                    foodCategoryId: 7,
                    foodItemId: 19
                },
                {
                    restaurantId: 8,
                    foodCategoryId: 7,
                    foodItemId: 20
                },
                {
                    restaurantId: 8,
                    foodCategoryId: 8,
                    foodItemId: 21
                },
                {
                    restaurantId: 9,
                    foodCategoryId: 3,
                    foodItemId: 9
                },
                {
                    restaurantId: 9,
                    foodCategoryId: 3,
                    foodItemId: 10
                },
                {
                    restaurantId: 9,
                    foodCategoryId: 3,
                    foodItemId: 11
                },
                {
                    restaurantId: 9,
                    foodCategoryId: 4,
                    foodItemId: 12
                },
                {
                    restaurantId: 9,
                    foodCategoryId: 4,
                    foodItemId: 13
                },
                {
                    restaurantId: 9,
                    foodCategoryId: 5,
                    foodItemId: 14
                },
                {
                    restaurantId: 9,
                    foodCategoryId: 5,
                    foodItemId: 15
                },
                {
                    restaurantId: 9,
                    foodCategoryId: 5,
                    foodItemId: 16
                },
                {
                    restaurantId: 9,
                    foodCategoryId: 6,
                    foodItemId: 17
                },
                {
                    restaurantId: 9,
                    foodCategoryId: 7,
                    foodItemId: 19
                },
                {
                    restaurantId: 9,
                    foodCategoryId: 7,
                    foodItemId: 20
                },
                {
                    restaurantId: 9,
                    foodCategoryId: 8,
                    foodItemId: 21
                },
                {
                    restaurantId: 10,
                    foodCategoryId: 3,
                    foodItemId: 9
                },
                {
                    restaurantId: 10,
                    foodCategoryId: 3,
                    foodItemId: 10
                },
                {
                    restaurantId: 10,
                    foodCategoryId: 3,
                    foodItemId: 11
                },
                {
                    restaurantId: 10,
                    foodCategoryId: 4,
                    foodItemId: 12
                },
                {
                    restaurantId: 10,
                    foodCategoryId: 4,
                    foodItemId: 13
                },
                {
                    restaurantId: 10,
                    foodCategoryId: 5,
                    foodItemId: 14
                },
                {
                    restaurantId: 10,
                    foodCategoryId: 5,
                    foodItemId: 15
                },
                {
                    restaurantId: 10,
                    foodCategoryId: 5,
                    foodItemId: 16
                },
                {
                    restaurantId: 10,
                    foodCategoryId: 6,
                    foodItemId: 17
                },
                {
                    restaurantId: 10,
                    foodCategoryId: 7,
                    foodItemId: 19
                },
                {
                    restaurantId: 10,
                    foodCategoryId: 7,
                    foodItemId: 20
                },
                {
                    restaurantId: 10,
                    foodCategoryId: 8,
                    foodItemId: 21
                }
            ],
            {}
        )
    },
    down: async (query) => {
        await query.dropTable('restaurants')
    }
}