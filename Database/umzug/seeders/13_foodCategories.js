const Sequelize = require('sequelize')

// All migrations must provide a `up` and `down` async functions

module.exports = {
    // `query` was passed in the `index.js` file
    up: async (queryInterface) => {
        const date = new Date();
        await queryInterface.bulkInsert(
            'food_categories',
            [
                {
                    id :1,
                    name: 'Recommended',
                },
                {
                    id :2,
                    name: 'New Arrivals'
                },
                {
                    id :3,
                    name: 'Classic Parathas'
                },
                {
                    id :4,
                    name: "Signature Parathas"
                },
                {
                    id :5,
                    name: "Curated Combos"
                },
                {
                    id :6,
                    name: 'Starters'
                },
                {
                    id :7,
                    name: 'Sides'
                },
                {
                    id :8,
                    name: 'Beverages'
                },
                {
                    id :9,
                    name: "Desserts"
                },
                {
                    id :10,
                    name: "Accompaniments"
                },
                {
                    id :11,
                    name: "Raitas & Salads"
                }
                
                
            ],
            {}
        )
    },
    down: async (query) => {
        await query.dropTable('restaurants')
    }
}