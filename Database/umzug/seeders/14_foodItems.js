const Sequelize = require('sequelize')

// All migrations must provide a `up` and `down` async functions

module.exports = {
    // `query` was passed in the `index.js` file
    up: async (queryInterface) => {
        await queryInterface.bulkInsert(
            'food_items',
            [
                {
                    id: 1,
                    name: 'Quickfire Combo for 1',
                    price: 269,
                    foodCategoryId: 1,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/c83/694ad48b2351ff5c66b8be7029369c83.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {   id: 2,
                    name: 'Balanced Paratha Box',
                    price: 299,
                    foodCategoryId: 1,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/e78/3dfb201bd1c707bd1731ee0d5c60ce78.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 3,
                    name: 'Spinach & Corn Paratha',
                    price: 209,
                    foodCategoryId: 1,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/d63/f2f0b40ac94360ff5cb0baec3c4bad63.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 4,
                    name: 'Achari Paneer Paratha',
                    price: 179,
                    foodCategoryId: 1,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/ec0/44a851000497fac9816968a793648ec0.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 5,
                    name: 'Aloo Methi Paratha',
                    price: 158,
                    foodCategoryId: 1,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/51e/6e3ca4ea08360ea579c1a259eebd151e.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 6,
                    name: 'Homely Dal Paratha',
                    price: 149,
                    foodCategoryId: 2,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/8c9/0d836713b0049869986b14300f5b38c9.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 7,
                    name: 'Pav Bhaji Paratha',
                    price: 189,
                    foodCategoryId: 2,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/fa1/492a3de423ed5bc02c0db7104f8f0fa1.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 8,
                    name: 'Paneer Bhurji Paratha',
                    price: 179,
                    foodCategoryId: 2,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/fcf/0ab5e25e980afc3699bc363eb6ba9fcf.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 9,
                    name: 'Aloo Methi Paratha',
                    price: 158,
                    foodCategoryId: 3,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/51e/6e3ca4ea08360ea579c1a259eebd151e.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 10,
                    name: 'Achari Paneer Paratha',
                    price: 179,
                    foodCategoryId: 3,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/ec0/44a851000497fac9816968a793648ec0.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 11,
                    name: 'Gobi Paratha',
                    price: 179,
                    foodCategoryId: 3,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/ec0/44a851000497fac9816968a793648ec0.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 12,
                    name: 'Jalapeno & Cheese Paratha',
                    price: 209,
                    foodCategoryId: 4,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/5c4/5ea32fbf3c8458e4c8ad0138c97d05c4.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 13,
                    name: 'Spinach & Corn Paratha',
                    price: 209,
                    foodCategoryId: 4,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/d63/f2f0b40ac94360ff5cb0baec3c4bad63.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 14,
                    name: 'Aloo Paratha + Plain Lassi',
                    price: 199,
                    foodCategoryId: 5,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/e78/3dfb201bd1c707bd1731ee0d5c60ce78.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 15,
                    name: 'Quickfire Combo for 1',
                    price: 269,
                    foodCategoryId: 5,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/c83/694ad48b2351ff5c66b8be7029369c83.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 16,
                    name: 'Balanced Paratha Box',
                    price: 199,
                    foodCategoryId: 5,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/203/10a67d58e0fe8805344aa722b6b57203.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 17,
                    name: 'Masaledaar Aloo Jeera',
                    price: 59,
                    foodCategoryId: 6,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/2a9/a48962b0ef10f16b77e6b9125c1aa2a9.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 18,
                    name: 'Dal Makhani (150 ml)',
                    price: 49,
                    foodCategoryId: 6,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/f43/8c753afa388284c5d693b6a7868c8f43.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 19,
                    name: 'Spinach & Corn Paratha',
                    price: 209,
                    foodCategoryId: 7,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/d63/f2f0b40ac94360ff5cb0baec3c4bad63.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 20,
                    name: 'Aloo Paratha + Plain Lassi',
                    price: 199,
                    foodCategoryId: 7,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/e78/3dfb201bd1c707bd1731ee0d5c60ce78.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 21,
                    name: 'Quickfire Combo for 1',
                    price: 269,
                    foodCategoryId: 8,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/c83/694ad48b2351ff5c66b8be7029369c83.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 22,
                    name: 'Balanced Paratha Box',
                    price: 199,
                    foodCategoryId: 9,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/203/10a67d58e0fe8805344aa722b6b57203.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 23,
                    name: 'Masaledaar Aloo Jeera',
                    price: 59,
                    foodCategoryId: 10,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/2a9/a48962b0ef10f16b77e6b9125c1aa2a9.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 24,
                    name: 'Dal Makhani (150 ml)',
                    price: 49,
                    foodCategoryId: 11,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/f43/8c753afa388284c5d693b6a7868c8f43.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 25,
                    name: 'Balanced Paratha Box',
                    price: 199,
                    foodCategoryId: 9,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/203/10a67d58e0fe8805344aa722b6b57203.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 26,
                    name: 'Masaledaar Aloo Jeera',
                    price: 59,
                    foodCategoryId: 10,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/2a9/a48962b0ef10f16b77e6b9125c1aa2a9.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                },
                {
                    id: 27,
                    name: 'Dal Makhani (150 ml)',
                    price: 49,
                    foodCategoryId: 11,
                    imageUrl: "https://b.zmtcdn.com/data/dish_photos/f43/8c753afa388284c5d693b6a7868c8f43.jpg?output-format=webp&fit=around|130:130&crop=130:130;*,*"
                }
                
            ],
            {}
        )
    },
    down: async (query) => {
        await query.dropTable('restaurants')
    }
}